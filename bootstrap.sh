sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get install -y python3-pip python3-virtualenvwrapper
sudo apt-get install -y ufw git
sudo apt-get install -y vim ntp nmap
pip install --user rich ptpython numpy
pip install --user pylint yapf autoflake isort coverage pyright
sudo ufw allow ssh
sudo ufw enable
curl https://sh.rustup.rs -sSf | sh
curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sh
sudo systemctl enable ntp
sudo systemctl start ntp
echo "export PATH=${PATH}:~/.cargo/bin" >> ~/.bashrc
source ~/.bashrc
curl -sLf https://spacevim.org/install.sh | bash
mkdir ~/.SpaceVim.d
cp ./base.toml .SpaceVim.d/init.toml
